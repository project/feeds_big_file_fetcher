<?php

/**
 * Result of FeedsBigFileFetcher::fetch().
 */
class FeedsBigFileFetcherResult extends FeedsFetcherResult {
  protected $url;

  protected $uri;


  /**
   * Constructor.
   */
  public function __construct($url = NULL, $uri = NULL) {
    $this->url = $url;
    $this->uri = $uri;
    $this->file_path = $uri;

    parent::__construct('');
  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    return $this->sanitizeRaw(file_get_contents($this->uri));
  }

  /**
   * Overrides parent::getFilePath().
   */
  public function getFilePath() {
    $this->checkFile();
    return $this->sanitizeFile($this->uri);
  }
}

/**
 * Fetches data via URL.
 */
class FeedsBigFileFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);

    return new FeedsBigFileFetcherResult($source_config['source'], $source_config['uri']);
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('Enter a feed URL. The file will be downloaded to URI.'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    $form['uri'] = array(
      '#type' => 'textfield',
      '#title' => t('URI'),
      '#description' => t('Enter the URI where the file is located. For example: private://feeds/bigfile.xml'),
      '#default_value' => isset($source_config['uri']) ? $source_config['uri'] : '',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    if ($values['source'] && !feeds_valid_url($values['source'], TRUE)) {
      $form_key = 'feeds][' . get_class($this) . '][source';
      form_set_error($form_key, t('The URL %source is invalid.', array('%source' => $values['source'])));
    }

    if (!file_valid_uri($values['uri'])) {
      $form_key = 'feeds][' . get_class($this) . '][uri';
      form_set_error($form_key, t('The URI %source is invalid.', array('%uri' => $values['uri'])));
    }
  }

  /**
   * Overrides parent::sourceSave().
   */
  public function sourceSave(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);

    if ($source_config['source'] && !isset($source_config['downloaded'])) {
      $path = drupal_realpath($source_config['uri']);
      $fp = fopen($path, 'w');

      $ch = curl_init($source_config['source']);

      // Download the file, not load it into memory.
      curl_setopt($ch, CURLOPT_FILE, $fp);

      curl_exec($ch);

      curl_close($ch);
      fclose($fp);

      $source_config['downloaded'] = TRUE;
      $source->setConfigFor($this, $source_config);
    }
  }
}
